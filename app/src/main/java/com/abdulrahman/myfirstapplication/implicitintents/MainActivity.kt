package com.abdulrahman.myfirstapplication.implicitintents

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.core.app.ShareCompat

class MainActivity : AppCompatActivity() {
    private lateinit var mWebsiteEditText:EditText
    private lateinit var mLocationEditText:EditText
    private lateinit var mShareTextEditText: EditText
//    private lateinit var
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mWebsiteEditText=findViewById(R.id.website_edittext)
        mLocationEditText=findViewById(R.id.location_edittext)
        mShareTextEditText=findViewById(R.id.share_edittext)

    }

    fun openWebsite(view: View) {
        val url=mWebsiteEditText.text.toString()
        val webPage=Uri.parse(url)
        val intent =Intent(Intent.ACTION_VIEW,webPage)
        if(intent.resolveActivity(packageManager)!=null){
            startActivity(intent)
        }else{
            Log.d("ImplicitIntents","Can't handle this!")
        }
    }

    fun openLocation(view: View) {
        val loc=mLocationEditText.text.toString()
        val addressesUri=Uri.parse("geo:0,0?q=$loc")
        val intent=Intent(Intent.ACTION_VIEW,addressesUri)
        intent.resolveActivity(packageManager)?.let{
            startActivity(intent)
        }?: Log.d("ImplicitIntents","can't handle this!")
    }
    fun shareText(view: View) {
        val txt=mShareTextEditText.text.toString()
        val mimeType="text/plain"
        ShareCompat.IntentBuilder.from(this).setType(mimeType).setChooserTitle("share this text with: ").setText(txt).startChooser();

    }
}
